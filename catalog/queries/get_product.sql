SELECT
*
FROM
catalog.product_variant

{% if sku %}
WHERE
  sku = '{{sku}}'
{% endif %}