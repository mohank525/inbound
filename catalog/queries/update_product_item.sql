INSERT INTO `catalog`.`product_variant` (
    `id_product_variant`,
    `sku`,
    `sku_config`,
    `title_en`,
    `brand_code`)
VALUES (%s, %s, %s, %s, %s);

-- INSERT INTO 'catalog'.'product_variant' ('id_product_variant','sku','sku_config','title_en','brand_code')VALUES ("1, "2", "3", "4", "5");