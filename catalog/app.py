from flask import Flask, render_template, json, request
# from flask.ext.mysql import MySQL
# from flask.ext.mysql import MySQL
from flaskext.mysql import MySQL
import os
import jinja2

tmpl_html = jinja2.Environment(
    autoescape=True,
    loader=jinja2.FileSystemLoader(os.path.join(os.path.dirname(__file__), 'templates')))

tmpl = jinja2.Environment(
    autoescape=True,
    loader=jinja2.FileSystemLoader(os.path.join(os.path.dirname(__file__), 'queries')))

mysql = MySQL()
app = Flask(__name__)

app.config['MYSQL_DATABASE_USER'] = 'root'
app.config['MYSQL_DATABASE_PASSWORD'] = 'login1-2'
app.config['MYSQL_DATABASE_DB'] = 'inbound'
app.config['MYSQL_DATABASE_HOST'] = 'localhost'
mysql.init_app(app)


def mysql_connection():
    print("========= cursor, conn start ==========")
    conn = mysql.connect()
    cur = conn.cursor()
    print ("========= cursor, conn generated ==========")
    return conn, cur

def mysql_disconnection(cursor, conn):
    print( "========= cursor, conn end =========="   )
    cursor.close() 
    conn.close()
    print( "========= cursor, conn destoryed ==========" )   
    return True

@app.route("/")
def home():
    return tmpl_html.get_template('index.html').render()


def get_data_from_db(query_template, scalar=False, **kwargs):
    try:
        conn, cur = mysql_connection()
        # import pdb
        # pdb.set_trace() 
        cur.execute(tmpl.get_template(query_template).render(**kwargs))
        result = cur.fetchall()
        cur.close()
        conn.close()
    except Exception as e:
        return json.dumps({'error':str(e)})

    # finally:
    #     mysql_disconnection(cursor, conn)    
    return json.dumps({'data': result})

def push_to_db(query_template, *args, **kwargs):
    try:
        conn, cur = mysql_connection()
        rendered_tmpl = tmpl.get_template(query_template).render(**kwargs)
        if 'many' in kwargs:
            data = cur.executemany(rendered_tmpl, *args)
        else:
            data = cur.execute(rendered_tmpl, *args)
        conn.commit()
        # cur.close()
        # conn.close()
        print(data)
        return json.dumps({'data': data})  
    except Exception as e:
        return json.dumps({'error':str(e)})

    finally:
        mysql_disconnection(conn, cur)    

#     '''connect to one of the avail pool and fetch data from provided sql template'''
#     async with app.pool['pin'].acquire() as conn:
#         async with conn.cursor(aiomysql.DictCursor) as cur:
#             await cur.execute(tmpl.get_template(query_template).render(**kwargs))
#             result = await cur.fetchall()
#             cur.close()
#         conn.close()
#     return result if not scalar or not result else result[0]

# invoice_number
# invoice_file
# purchase_order
# partner

# CREATE TABLE inbound_qc( invoice_number, invoice_file, purchase_order,partner );

# INSERT INTO inbound_qc(invoice_number, invoice_file, purchase_order,partner)VALUES (12, 45, 78, 56);